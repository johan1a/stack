FROM ubuntu:18.10

RUN apt update -y \
  && apt install -y curl libcurl4-openssl-dev libpq-dev

RUN curl -sSL https://get.haskellstack.org/ | sh

WORKDIR /app

ENTRYPOINT ["stack"]
